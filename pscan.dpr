program pscan;

uses
  Vcl.Forms,
  form_main in 'form_main.pas' {PulsarScanerMainForm},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Pulsar scaner';
  TStyleManager.TrySetStyle('Luna');
  Application.CreateForm(TPulsarScanerMainForm, PulsarScanerMainForm);
  Application.Run;
end.


{$define DEBUG}
unit form_main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.PG,
  FireDAC.Phys.PGDef, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Vcl.Grids,
  Vcl.DBGrids, FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  Vcl.DBCtrls, Vcl.ComCtrls, IPPeerClient, REST.Client, Data.Bind.Components,
  Data.Bind.ObjectScope, REST.Types, ZDataset, ZAbstractRODataset,
  ZAbstractDataset, ZAbstractTable, ZAbstractConnection, ZConnection,
  Vcl.ExtCtrls, Vcl.ImgList, IdBaseComponent, IdComponent, IdCustomTCPServer,
  IdCustomHTTPServer, IdHTTPServer, IdContext;

type
  TPulsarScanerMainForm = class(TForm)
    btnGetAddresses: TButton;
    btnGetLastValues: TButton;
    dsAddresses: TDataSource;
    dbgAddresses: TDBGrid;
    lblAddresses: TLabel;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    NameMemo: TDBMemo;
    lblName: TLabel;
    CommentMemo: TDBMemo;
    lblComment: TLabel;
    dsLastData: TDataSource;
    dbgLastData: TDBGrid;
    lblLastValues: TLabel;
    dtpCurrentDate: TDateTimePicker;
    btnSendData: TButton;
    DataSaverRESTClient: TRESTClient;
    DataSaverRESTRequest: TRESTRequest;
    DataSaverRESTResponse: TRESTResponse;
    lblLog: TLabel;
    mmoLog: TMemo;
    conPulsar: TZConnection;
    ztblAddresses: TZTable;
    ztblAddressesplc_id: TIntegerField;
    ztblAddressesName: TWideMemoField;
    ztblAddressesComment: TWideMemoField;
    ztblAddressestyp_id: TIntegerField;
    ztblAddressesCod_plc: TIntegerField;
    ztblAddressesplace_id: TIntegerField;
    zqryLastData: TZQuery;
    zqryLastDataprp_id: TIntegerField;
    zqryLastDataplc_id: TIntegerField;
    zqryLastDataNameGroup: TWideMemoField;
    zqryLastDataComment: TWideMemoField;
    zqryLastDataDataValue: TFloatField;
    zqryLastDatatyp_arh: TIntegerField;
    zqryLastDataDateValue: TDateTimeField;
    pbSending: TProgressBar;
    DBNavigator1: TDBNavigator;
    DBNavigator2: TDBNavigator;
    btndtSearch: TButtonedEdit;
    ilFuncs: TImageList;
    lblSearch: TLabel;
    zqryLastDataName: TWideMemoField;
    btnStopWebService: TButton;
    btnStartWebService: TButton;
    IdHTTPServer: TIdHTTPServer;
    zqryAllDataToDate: TZQuery;
    dsAllDataToDate: TDataSource;
    dbg1: TDBGrid;
    btnQueryAllData: TButton;
    zqryAllDataToDateprp_id: TIntegerField;
    zqryAllDataToDateplc_id: TIntegerField;
    zqryAllDataToDateNameGroup: TWideMemoField;
    zqryAllDataToDateComment: TWideMemoField;
    zqryAllDataToDateDataValue: TFloatField;
    zqryAllDataToDatetyp_arh: TIntegerField;
    zqryAllDataToDateDateValue: TDateTimeField;
    zqryAllDataToDateName: TWideMemoField;
    btnClearLog: TButton;
    procedure btnGetAddressesClick(Sender: TObject);
    procedure btnGetLastValuesClick(Sender: TObject);
    procedure btnSendDataClick(Sender: TObject);
    procedure DataSaverRESTRequestAfterExecute(Sender: TCustomRESTRequest);
    procedure FormCreate(Sender: TObject);
    procedure dtpCurrentDateChange(Sender: TObject);
    procedure ztblAddressesNameGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure ztblAddressesCommentGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure zqryLastDataCommentGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure zqryLastDataNameGroupGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure btndtSearchRightButtonClick(Sender: TObject);
    procedure btndtSearchKeyPress(Sender: TObject; var Key: Char);
    procedure zqryLastDataNameGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure btnStartWebServiceClick(Sender: TObject);
    procedure btnStopWebServiceClick(Sender: TObject);
    procedure IdHTTPServerCommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure btnQueryAllDataClick(Sender: TObject);
    procedure btnClearLogClick(Sender: TObject);
  private
    cachedJSONResponse: string;
    CachedValues: string;
    oldQueryDate : TDateTime;
    FDateChanged: Boolean;
    function GetValueJson: String;
    function EncodeAsUTF8(unicodeStr: string): string;
    function GetAllValueJson: String;
    function GetLastValueJson: String;
    procedure SetDateChanged(const Value: Boolean);
    procedure WriteLog(AMessage: string);
    procedure FindAddress;
    procedure SelectSearchEdit;
    function RemoveQuotation(value: string): string;
    procedure ConnectToDatabase;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property DateChanged : Boolean read FDateChanged write SetDateChanged;
  end;

var
  PulsarScanerMainForm: TPulsarScanerMainForm;

implementation

{$R *.dfm}

procedure TPulsarScanerMainForm.btnClearLogClick(Sender: TObject);
begin
  mmoLog.Lines.Clear;
end;

procedure TPulsarScanerMainForm.btndtSearchKeyPress(Sender: TObject;
  var Key: Char);
begin
    case Key of
        #13: begin
                FindAddress();
                SelectSearchEdit();
            end;
    end;
end;

procedure TPulsarScanerMainForm.btndtSearchRightButtonClick(Sender: TObject);
begin
  FindAddress();
  SelectSearchEdit();
end;

procedure TPulsarScanerMainForm.SelectSearchEdit();
begin
    btndtSearch.SelectAll;
end;

procedure TPulsarScanerMainForm.FindAddress();
    var
        recId : Integer;
begin
    ztblAddresses.First();
    ztblAddresses.Filtered := false;
    ztblAddresses.FilterOptions := [foCaseInsensitive];
    ztblAddresses.Filter := ('Name LIKE ' + QuotedStr( '*' + btndtSearch.Text + '*' ) );
    ztblAddresses.Filtered := true;

    WriteLog( ztblAddresses.Filter );

    exit;

    if( ztblAddresses.RecordCount = 0 )then
    begin
        WriteLog( 'Not found ' + btndtSearch.Text );
        ztblAddresses.Filtered := false;
    end
    else
    begin
      recId := ztblAddressesplc_id.AsInteger;
      ztblAddresses.Filtered := false;
      ztblAddresses.First;
      ztblAddresses.Locate('plc_id', recId, [] );
    end;
end;

procedure TPulsarScanerMainForm.WriteLog( AMessage: string );
    const
        LOG_SIZE = 100;
begin
  mmoLog.Lines.Add( DateTimeToStr(Now)+ ':' + AMessage );
  if( mmoLog.Lines.Count > LOG_SIZE )then
    mmoLog.Lines.Delete(0);
end;

procedure TPulsarScanerMainForm.btnGetAddressesClick(Sender: TObject);
begin
    ztblAddresses.Active := true;
end;

procedure TPulsarScanerMainForm.btnGetLastValuesClick(Sender: TObject);
begin
    zqryLastData.Close();
    zqryLastData.ParamByName('Date_Parm').Value := dtpCurrentDate.Date;
    zqryLastData.Active := true;
end;

procedure TPulsarScanerMainForm.btnQueryAllDataClick(Sender: TObject);
begin
  zqryAllDataToDate.Close();
  zqryAllDataToDate.ParamByName('Date_Parm').Value := dtpCurrentDate.Date;
  zqryAllDataToDate.Open();
end;

procedure TPulsarScanerMainForm.btnSendDataClick(Sender: TObject);
begin
    btnSendData.Enabled := false;
    try
      if( DateChanged )or( CachedValues = '' )then
      begin
          DateChanged := false;
          CachedValues := GetAllValueJson();
      end;

      DataSaverRESTRequest.Params.Clear();
      DataSaverRESTRequest.Params.AddItem( 'value', CachedValues, pkREQUESTBODY, [], TRESTContentType.ctAPPLICATION_JSON );
      DataSaverRESTRequest.Execute();

    finally
        btnSendData.Enabled := true;
    end;
end;

procedure TPulsarScanerMainForm.btnStartWebServiceClick(Sender: TObject);
begin
  IdHTTPServer.Active := true;
  btnStopWebService.Enabled := true;
  btnStartWebService.Enabled := false;
end;

procedure TPulsarScanerMainForm.btnStopWebServiceClick(Sender: TObject);
begin
  IdHTTPServer.Active := false;
  btnStopWebService.Enabled := false;
  btnStartWebService.Enabled := true;
end;

constructor TPulsarScanerMainForm.Create(AOwner: TComponent);
begin
  inherited;

  oldQueryDate := 0;
  cachedJSONResponse := '';

end;

function TPulsarScanerMainForm.GetLastValueJson(): String;
var
  LastVals: string;
begin
  LastVals := '';
  zqryLastData.DisableControls();

  zqryLastData.First();
  while not zqryLastData.Eof do
  begin
    LastVals := LastVals + GetValueJson();

    zqryLastData.Next();
    if( not zqryLastData.Eof )and( LastVals <> '' )then
      LastVals := LastVals + ',';
  end;

  zqryLastData.EnableControls();

  Result := LastVals;
end;

function TPulsarScanerMainForm.GetAllValueJson(): String;
    const
        NewLine = #13#10;
    var
        LastVals : string;
        PrevVals : string;
        outFile : TFileStream;
begin
    ztblAddresses.BlockReadSize := 100;
    zqryLastData.BlockReadSize := 100;

    pbSending.Max := ztblAddresses.RecordCount;
    pbSending.Position := 1;
    pbSending.Step := 1;

    Result := '[';
    ztblAddresses.First();
    LastVals := '';
    while not ztblAddresses.Eof do
    begin
        PrevVals := LastVals;
        LastVals := GetLastValueJson();

        if( LastVals = '' )then
            Result := Result + NewLine;

        Result := Result + LastVals;

        ztblAddresses.Next();
        if( not ztblAddresses.Eof )and( LastVals <> '' )then
          Result := Result + ',';

        pbSending.StepIt();
        Application.ProcessMessages();
    end;

    if( Result[Length(Result)] = ',' )then
        Result[Length(Result)] := ' ' ;
    Result := Result + ']';

    zqryLastData.BlockReadSize := 0;
    ztblAddresses.BlockReadSize := 0;
end;

function TPulsarScanerMainForm.GetValueJson(): String;
begin
    Result := '{'+
           '"id":"' + ztblAddressesplc_id.AsString +
        '", "prp_id":"' + zqryLastDataprp_id.AsString +
        '", "typ_arh":"' + zqryLastDatatyp_arh.AsString +
        '", "date_query":"' + zqryLastDataDateValue.AsString +
        '", "value":"' + zqryLastDataDataValue.AsString +
        '", "name":"' + RemoveQuotation( zqryLastDataName.AsString ) +
        '", "name_group":"' + zqryLastDataNameGroup.AsString +
        '"}';
end;

procedure TPulsarScanerMainForm.ConnectToDatabase();
begin
  conPulsar.Connect;
  ztblAddresses.Open();
end;

procedure TPulsarScanerMainForm.IdHTTPServerCommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
  var
    res : string;
begin
  dtpCurrentDate.DateTime := Now;

  WriteLog('Client connected... calculating');
  WriteLog( 'Cmd:' + ARequestInfo.Command );
  WriteLog( 'Params:' + ARequestInfo.Params.Text );
  WriteLog( 'URI:' + ARequestInfo.URI );
  WriteLog( 'Doc:' + ARequestInfo.Document );

  ConnectToDatabase();

  if( ARequestInfo.Params.IndexOfName('date') > -1 )then
  begin
    dtpCurrentDate.Date := StrToDate(ARequestInfo.Params.Values['date']);
  end;


  if( oldQueryDate <> dtpCurrentDate.DateTime )then
  begin
    WriteLog('Dropped cache');
    oldQueryDate := dtpCurrentDate.DateTime;
    cachedJSONResponse := '';
  end;

  if( cachedJSONResponse = '' )and(ARequestInfo.URI = '/last-values.json')then
  begin

    btnGetAddressesClick(self);
    btnGetLastValuesClick(self);

    ztblAddresses.BlockReadSize := 1;
    zqryLastData.BlockReadSize := 1;

    res := '{"meters":[';

    ztblAddresses.First();
    while not ztblAddresses.Eof do
    begin
      res := res + '{"name":"' + RemoveQuotation( ztblAddressesName.AsString ) + '",' +
        '"plc_id":' + ztblAddressesplc_id.AsString + ',';

      if( zqryLastData.RecordCount > 0 )then
      begin
        res := res + '"prp_id":' + zqryLastDataprp_id.AsString + ',' +
          '"value":' + IntToStr(zqryLastDataDataValue.AsInteger) + ',' +
          '"date":"' + DateTimeToStr(zqryLastDataDateValue.Value)+ '",' ;
        ;
      end;

      res := res + '"typ_id":' + ztblAddressestyp_id.AsString +
        '}'
      ;

      ztblAddresses.Next;

      if( not ztblAddresses.Eof )then
      begin
        res := res + ',';
      end;

    end;
    res := res + ']}';
    cachedJSONResponse := res;
    zqryLastData.BlockReadSize := 0;
    ztblAddresses.BlockReadSize := 0;

    ztblAddresses.First();
  end
  else
  begin
    WriteLog('Uses cached values');
  end;



  AResponseInfo.ContentText := '';

  if(ARequestInfo.URI = '/last-values.json')then
    AResponseInfo.ContentText := cachedJSONResponse;
  AResponseInfo.ContentType := 'text/json';
  AResponseInfo.ContentEncoding := 'utf-8';
  AResponseInfo.CharSet :='utf-8';

  WriteLog('Data sent.');
  WriteLog('Data:' + Copy( cachedJSONResponse, 1, 100 ) + '...' );
  WriteLog('Data len:' + IntToStr(Length( cachedJSONResponse )));
end;

function TPulsarScanerMainForm.RemoveQuotation( value : string ) : string;
  var
    i : integer;
begin
  Result := '';
  for i := 1 to Length(value) do
    if( value[i] <> '"' )then
      Result := result + value[i];
end;

procedure TPulsarScanerMainForm.SetDateChanged(const Value: Boolean);
begin
  FDateChanged := Value;
end;

procedure TPulsarScanerMainForm.zqryLastDataCommentGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
    Text := zqryLastDataComment.AsString;
end;

procedure TPulsarScanerMainForm.zqryLastDataNameGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := zqryLastDataName.AsString;
end;

procedure TPulsarScanerMainForm.zqryLastDataNameGroupGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
    Text := zqryLastDataNameGroup.AsString;
end;

procedure TPulsarScanerMainForm.ztblAddressesCommentGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
    Text := ztblAddressesComment.AsString;
end;

procedure TPulsarScanerMainForm.ztblAddressesNameGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
    Text := ztblAddressesName.AsString;
end;

function TPulsarScanerMainForm.EncodeAsUTF8( unicodeStr : string ): string;
    var
        UTF8Str : UTF8String;
        I : integer;
begin
    UTF8Str := UTF8String(unicodeStr);
    SetLength(Result, Length(UTF8Str));
    for i := 1 to Length(UTF8Str) do
      Result[i] := Char(Ord(Utf8Str[i]));
end;

procedure TPulsarScanerMainForm.FormCreate(Sender: TObject);
begin
    {$IFDEF DEBUG}
        dtpCurrentDate.Date := StrToDate('08.11.2014');
    {$ENDIF}
end;

procedure TPulsarScanerMainForm.DataSaverRESTRequestAfterExecute(
  Sender: TCustomRESTRequest);
begin
    mmoLog.Lines.Add( DataSaverRESTResponse.Content );
end;

procedure TPulsarScanerMainForm.dtpCurrentDateChange(Sender: TObject);
begin
    DateChanged := true;
end;

end.
